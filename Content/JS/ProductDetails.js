﻿"use strict";
function addItemSuccessCallback(data) {
    console.log("Add Item Result: ");
    console.log(data);

    if (data) {
        // successfully added

        // add 1 dynamically to number of items in cart in header
        $("#cart-qty").html(parseInt($("#cart-qty").html()) + parseInt($("#item-quantity").val()));

        $(document).trigger('UpdateShoppingCart', [data]);

    } else {
        alert("An error occurred while adding your item to the cart, please refresh the page and try again");
    }
}

function addItemFailureCallback(data) {
    var errorElement = $("#body-wrapper").find(".msg-error");
    if (data.responseJSON && data.responseJSON.length > 0 && data.responseJSON[0].LocalizedErrorMessage) {        
        errorElement[0].textContent = data.responseJSON[0].LocalizedErrorMessage;
    } else {
        errorElement[0].textContent = "An error occurred while adding your item to the cart, please refresh the page and try again";
    }
}

function addLinesToWishListSuccessCallback(data) {
    console.log("Add Item Result: ");
    console.log(data);

    if (data) {
        $("#AddToWishListModal").modal('hide');
        var errorElement = $("#body-wrapper").find(".msg-info");
        errorElement[0].textContent = "You have succesfully added this product to your wish list."
    } else {
        alert("An error occurred while adding your item to the wish list, please refresh the page and try again");
    }
}

function addLinesToWishListFailureCallback(data) {
    var errorElement = $("#body-wrapper").find(".msg-error");
    if (data.responseJSON && data.responseJSON.length > 0 && data.responseJSON[0].LocalizedErrorMessage) {
        errorElement[0].textContent = data.responseJSON[0].LocalizedErrorMessage;
    } else {
        errorElement[0].textContent = "An error occurred while adding your item to the wish list, please refresh the page and try again";
    }
}

function isAuthenticatedSessionSuccessCallback(data) {
    if (!data) {
        $("#add-to-wishList-btn").hide();
    }
}

function isAuthenticatedSessionFailureCallback(data) {
    var errorElement = $("#body-wrapper").find(".msg-error");
    if (data.responseJSON && data.responseJSON.length > 0 && data.responseJSON[0].LocalizedErrorMessage) {
        errorElement[0].textContent = data.responseJSON[0].LocalizedErrorMessage;
    } else {
        errorElement[0].textContent = "An unexpected error has occured, please refresh the page";
    }
}

function createWishListSuccessCallback(data) {
    console.log("Add Item Result: ");
    console.log(data);

    if (data) {
        var productId = $("#product-details-product-id").val();
        if (productId != 0 && productId != undefined) {
            var wishListLine = { "ProductId": productId, "Quantity": $("#item-quantity").val(), "Price": ($("#giftcardAmount") != null) ? $("#giftcardAmount").val() : null };
            var data = {
                "wishListId": data.Id,
                "wishListLines": [wishListLine]
            };

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: "/WishList/AddLinesToWishList",
                data: JSON.stringify(data),
                success: addLinesToWishListSuccessCallback,
                error: addLinesToWishListFailureCallback
            });
        }
        else {
            var errorElement = $("#body-wrapper").find(".msg-error");
            errorElement[0].textContent = "No such product";
        }
    } else {
        alert("An error occurred while adding your item to the wish list, please refresh the page and try again");
    }
}

function createWishListFailureCallback(data) {
    var errorElement = $("#body-wrapper").find(".msg-error");
    if (data.responseJSON && data.responseJSON.length > 0 && data.responseJSON[0].LocalizedErrorMessage) {
        errorElement[0].textContent = data.responseJSON[0].LocalizedErrorMessage;
    } else {
        errorElement[0].textContent = "An error occurred while creating your wish list, please refresh the page and try again";
    }
}

function getWishListsSuccessCallback(data) {
    $leftScrollButton = $(document).find('.left-scroll-list .msax-Button');
    $rightScrollButton = $(document).find('.right-scroll-list .msax-Button');
    $WishListAddTilesContainer = $(document).find('.msax-WishListTilesContainer');
    $noWishListsText = $(document).find('.no-wishlists-text');
    $WishListAddTilesContainer.empty();
    $WishListAddTilesContainer.prev().show();
    $WishListAddTilesContainer.next().show();
    if (data.length <= 3) {
        $leftScrollButton.hide();
        $rightScrollButton.hide();
    }
    else {
        $leftScrollButton.show();
        $rightScrollButton.show();
    }
    if (data.length == 0) {
        $noWishListsText.show();
    }
    else {
        $noWishListsText.hide();
    }
    for (var i = 0; i < data.length; i++) {
        var wishList = data[i];
        var tileMarkup = null;
        if (wishList.IsFavorite == true) {
            tileMarkup = $('<div class="tile-view-content"><a href="javascript:void(0);" class="msax-ListTile" id="valueId:null"><div class="tile-view-details-box"></div></a><span class="msax-Star"><b>&#9733;</b></span></div>');
        } else {
            tileMarkup = $('<div class="tile-view-content"><a href="javascript:void(0);" class="msax-ListTile" id="valueId:null"><div class="tile-view-details-box"></div></a></div>');
        }
        var $tileMarkupText = tileMarkup.find('.tile-view-details-box');
        $tileMarkupText.text(wishList.Name);
        var $tileListId = tileMarkup.find('.msax-ListTile');
        $tileListId.val(wishList.Id);
        $WishListAddTilesContainer.append(tileMarkup);
    }

    // if the click event handler has already been bound we want to unbind it first
    $leftScrollButton.unbind("click");
    $rightScrollButton.unbind("click");

    $leftScrollButton.click(function (event) {
        event.preventDefault();
        var $firstVisibleTile, $prevTile, $nextTile;
        $firstVisibleTile = $prevTile = $nextTile = $WishListAddTilesContainer.find('.msax-Visible').first();
        var leftScrollPossible = true;

        // Show the previous three tiles
        for (var i = 0; i < 3; i++) {
            $prevTile = $prevTile.prev();

            if ($prevTile.length > 0) {
                $prevTile.show();
                $prevTile.addClass('msax-Visible');
            } else {
                leftScrollPossible = false;
                return false;
            }
        }

        // Hide the current three tiles only if a previous window of three tiles were made visible.
        if (leftScrollPossible) {
            for (var i = 0; i < 3; i++) {
                if ($nextTile.length > 0) {
                    $nextTile.hide();
                    $nextTile.removeClass('msax-Visible');
                } else {
                    return false;
                }

                $nextTile = $nextTile.next();
            }
        }
    });

    $rightScrollButton.click(function (event) {
        event.preventDefault();
        var $lastVisibleTile, $prevTile, $nextTile;
        $lastVisibleTile = $prevTile = $nextTile = $WishListAddTilesContainer.find('.msax-Visible').last();
        var rightScrollPossible = true;
        var first = true;

        // Show the next three tiles
        for (var i = 0; i < 3; i++) {
            $nextTile = $nextTile.next();

            if ($nextTile.length > 0) {
                $nextTile.show();
                $nextTile.addClass('msax-Visible');
                first = false;
            } else {
                if (first) {
                    first = false; // This is required for right scroll because if there is no tile on the right side it means righ scoll is not possible, but rightmost window could have just 1/2 tiles.
                    rightScrollPossible = false;
                    return false;
                }
            }
        }

        // Hide the current three tiles only if a next window of three tiles were made visible.
        if (rightScrollPossible) {
            for (var i = 0; i < 3; i++) {
                if ($prevTile.length > 0) {
                    $prevTile.hide();
                    $prevTile.removeClass('msax-Visible');
                } else {
                    return false;
                }

                $prevTile = $prevTile.prev();
            }
        }
    });

    // Show the first three tiles on first time load
    $WishListAddTilesContainer.find('.tile-view-content').each(function (index) {
        if (index > 2) {
            $(this).hide();
        } else {
            $(this).addClass('msax-Visible');
        }
    });

    $(".msax-ListTile").click(function () {
        var tile = $(this);
        var productId = $("#product-details-product-id").val();
        if (productId != 0 && productId != undefined) {
            var wishListLine = { "ProductId": productId, "Quantity": $("#item-quantity").val(), "Price": ($("#giftcardAmount") != null) ? $("#giftcardAmount").val() : null };
            var data = {
                "wishListId": tile.val(),
                "wishListLines": [wishListLine]
            };

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: "/WishList/AddLinesToWishList",
                data: JSON.stringify(data),
                success: addLinesToWishListSuccessCallback,
                error: addLinesToWishListFailureCallback
            });
        }
        else {
            var errorElement = $("#body-wrapper").find(".msg-error");
            errorElement[0].textContent = "No such product";
        }
    });
}

function getWishListsFailureCallback(data) {
    var errorElement = $("#body-wrapper").find(".msg-error");
    if (data.responseJSON && data.responseJSON.length > 0 && data.responseJSON[0].LocalizedErrorMessage) {
        errorElement[0].textContent = data.responseJSON[0].LocalizedErrorMessage;
    } else {
        errorElement[0].textContent = "An error occurred while getting your wish lists, please refresh the page and try again";
    }
}

$("#plus-qty").click(function () {
    var inputVal = parseInt($("#item-quantity").val());
    if (!isNaN(inputVal))
    {
        inputVal++;
    }

    $("#item-quantity").val(inputVal);
    $("#item-quantity").trigger("change");
});

$("#minus-qty").click(function () {
    var inputVal = parseInt($("#item-quantity").val());
    if (!isNaN(inputVal)) {
        inputVal--;
    }

    $("#item-quantity").val(inputVal);
    $("#item-quantity").trigger("change");
});

$("#item-quantity").change(function () {
    var inputVal = parseInt($("#item-quantity").val());
    if (inputVal <= 1 || isNaN(inputVal)) {
        inputVal = 1;
        $("#plus-qty").prop("disabled", false);
        $("#minus-qty").prop("disabled", true);
    }
    else if (inputVal >= 999)
    {
        inputVal = 999;
        $("#plus-qty").prop("disabled", true);
        $("#minus-qty").prop("disabled", false);
    }
    else
    {
        $("#plus-qty").prop("disabled", false);
        $("#minus-qty").prop("disabled", false);
    }

    $("#item-quantity").val(inputVal);
});
// SIG // Begin signature block
// SIG // MIInuAYJKoZIhvcNAQcCoIInqTCCJ6UCAQExDzANBglg
// SIG // hkgBZQMEAgEFADB3BgorBgEEAYI3AgEEoGkwZzAyBgor
// SIG // BgEEAYI3AgEeMCQCAQEEEBDgyQbOONQRoqMAEEvTUJAC
// SIG // AQACAQACAQACAQACAQAwMTANBglghkgBZQMEAgEFAAQg
// SIG // kGGbIUHYGq5r+Jh56iu5kNbZFP+er8m2YUWy2qy60uCg
// SIG // gg2BMIIF/zCCA+egAwIBAgITMwAAAlKLM6r4lfM52wAA
// SIG // AAACUjANBgkqhkiG9w0BAQsFADB+MQswCQYDVQQGEwJV
// SIG // UzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMH
// SIG // UmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSgwJgYDVQQDEx9NaWNyb3NvZnQgQ29kZSBT
// SIG // aWduaW5nIFBDQSAyMDExMB4XDTIxMDkwMjE4MzI1OVoX
// SIG // DTIyMDkwMTE4MzI1OVowdDELMAkGA1UEBhMCVVMxEzAR
// SIG // BgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1v
// SIG // bmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlv
// SIG // bjEeMBwGA1UEAxMVTWljcm9zb2Z0IENvcnBvcmF0aW9u
// SIG // MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
// SIG // 0OTPj7P1+wTbr+Qf9COrqA8I9DSTqNSq1UKju4IEV3HJ
// SIG // Jck61i+MTEoYyKLtiLG2Jxeu8F81QKuTpuKHvi380gzs
// SIG // 43G+prNNIAaNDkGqsENQYo8iezbw3/NCNX1vTi++irdF
// SIG // qXNs6xoc3B3W+7qT678b0jTVL8St7IMO2E7d9eNdL6RK
// SIG // fMnwRJf4XfGcwL+OwwoCeY9c5tvebNUVWRzaejKIkBVT
// SIG // hApuAMCtpdvIvmBEdSTuCKZUx+OLr81/aEZyR2jL1s2R
// SIG // KaMz8uIzTtgw6m3DbOM4ewFjIRNT1hVQPghyPxJ+ZwEr
// SIG // wry5rkf7fKuG3PF0fECGSUEqftlOptpXTQIDAQABo4IB
// SIG // fjCCAXowHwYDVR0lBBgwFgYKKwYBBAGCN0wIAQYIKwYB
// SIG // BQUHAwMwHQYDVR0OBBYEFDWSWhFBi9hrsLe2TgLuHnxG
// SIG // F3nRMFAGA1UdEQRJMEekRTBDMSkwJwYDVQQLEyBNaWNy
// SIG // b3NvZnQgT3BlcmF0aW9ucyBQdWVydG8gUmljbzEWMBQG
// SIG // A1UEBRMNMjMwMDEyKzQ2NzU5NzAfBgNVHSMEGDAWgBRI
// SIG // bmTlUAXTgqoXNzcitW2oynUClTBUBgNVHR8ETTBLMEmg
// SIG // R6BFhkNodHRwOi8vd3d3Lm1pY3Jvc29mdC5jb20vcGtp
// SIG // b3BzL2NybC9NaWNDb2RTaWdQQ0EyMDExXzIwMTEtMDct
// SIG // MDguY3JsMGEGCCsGAQUFBwEBBFUwUzBRBggrBgEFBQcw
// SIG // AoZFaHR0cDovL3d3dy5taWNyb3NvZnQuY29tL3BraW9w
// SIG // cy9jZXJ0cy9NaWNDb2RTaWdQQ0EyMDExXzIwMTEtMDct
// SIG // MDguY3J0MAwGA1UdEwEB/wQCMAAwDQYJKoZIhvcNAQEL
// SIG // BQADggIBABZJN7ksZExAYdTbQJewYryBLAFnYF9amfhH
// SIG // WTGG0CmrGOiIUi10TMRdQdzinUfSv5HHKZLzXBpfA+2M
// SIG // mEuJoQlDAUflS64N3/D1I9/APVeWomNvyaJO1mRTgJoz
// SIG // 0TTRp8noO5dJU4k4RahPtmjrOvoXnoKgHXpRoDSSkRy1
// SIG // kboRiriyMOZZIMfSsvkL2a5/w3YvLkyIFiqfjBhvMWOj
// SIG // wb744LfY0EoZZz62d1GPAb8Muq8p4VwWldFdE0y9IBMe
// SIG // 3ofytaPDImq7urP+xcqji3lEuL0x4fU4AS+Q7cQmLq12
// SIG // 0gVbS9RY+OPjnf+nJgvZpr67Yshu9PWN0Xd2HSY9n9xi
// SIG // au2OynVqtEGIWrSoQXoOH8Y4YNMrrdoOmjNZsYzT6xOP
// SIG // M+h1gjRrvYDCuWbnZXUcOGuOWdOgKJLaH9AqjskxK76t
// SIG // GI6BOF6WtPvO0/z1VFzan+2PqklO/vS7S0LjGEeMN3Ej
// SIG // 47jbrLy3/YAZ3IeUajO5Gg7WFg4C8geNhH7MXjKsClsA
// SIG // Pk1YtB61kan0sdqJWxOeoSXBJDIzkis97EbrqRQl91K6
// SIG // MmH+di/tolU63WvF1nrDxutjJ590/ALi383iRbgG3zkh
// SIG // EceyBWTvdlD6FxNbhIy+bJJdck2QdzLm4DgOBfCqETYb
// SIG // 4hQBEk/pxvHPLiLG2Xm9PEnmEDKo1RJpMIIHejCCBWKg
// SIG // AwIBAgIKYQ6Q0gAAAAAAAzANBgkqhkiG9w0BAQsFADCB
// SIG // iDELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEyMDAGA1UEAxMpTWlj
// SIG // cm9zb2Z0IFJvb3QgQ2VydGlmaWNhdGUgQXV0aG9yaXR5
// SIG // IDIwMTEwHhcNMTEwNzA4MjA1OTA5WhcNMjYwNzA4MjEw
// SIG // OTA5WjB+MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2Fz
// SIG // aGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UE
// SIG // ChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSgwJgYDVQQD
// SIG // Ex9NaWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQSAyMDEx
// SIG // MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA
// SIG // q/D6chAcLq3YbqqCEE00uvK2WCGfQhsqa+laUKq4Bjga
// SIG // BEm6f8MMHt03a8YS2AvwOMKZBrDIOdUBFDFC04kNeWSH
// SIG // fpRgJGyvnkmc6Whe0t+bU7IKLMOv2akrrnoJr9eWWcpg
// SIG // GgXpZnboMlImEi/nqwhQz7NEt13YxC4Ddato88tt8zpc
// SIG // oRb0RrrgOGSsbmQ1eKagYw8t00CT+OPeBw3VXHmlSSnn
// SIG // Db6gE3e+lD3v++MrWhAfTVYoonpy4BI6t0le2O3tQ5GD
// SIG // 2Xuye4Yb2T6xjF3oiU+EGvKhL1nkkDstrjNYxbc+/jLT
// SIG // swM9sbKvkjh+0p2ALPVOVpEhNSXDOW5kf1O6nA+tGSOE
// SIG // y/S6A4aN91/w0FK/jJSHvMAhdCVfGCi2zCcoOCWYOUo2
// SIG // z3yxkq4cI6epZuxhH2rhKEmdX4jiJV3TIUs+UsS1Vz8k
// SIG // A/DRelsv1SPjcF0PUUZ3s/gA4bysAoJf28AVs70b1FVL
// SIG // 5zmhD+kjSbwYuER8ReTBw3J64HLnJN+/RpnF78IcV9uD
// SIG // jexNSTCnq47f7Fufr/zdsGbiwZeBe+3W7UvnSSmnEyim
// SIG // p31ngOaKYnhfsi+E11ecXL93KCjx7W3DKI8sj0A3T8Hh
// SIG // hUSJxAlMxdSlQy90lfdu+HggWCwTXWCVmj5PM4TasIgX
// SIG // 3p5O9JawvEagbJjS4NaIjAsCAwEAAaOCAe0wggHpMBAG
// SIG // CSsGAQQBgjcVAQQDAgEAMB0GA1UdDgQWBBRIbmTlUAXT
// SIG // gqoXNzcitW2oynUClTAZBgkrBgEEAYI3FAIEDB4KAFMA
// SIG // dQBiAEMAQTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUw
// SIG // AwEB/zAfBgNVHSMEGDAWgBRyLToCMZBDuRQFTuHqp8cx
// SIG // 0SOJNDBaBgNVHR8EUzBRME+gTaBLhklodHRwOi8vY3Js
// SIG // Lm1pY3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0cy9N
// SIG // aWNSb29DZXJBdXQyMDExXzIwMTFfMDNfMjIuY3JsMF4G
// SIG // CCsGAQUFBwEBBFIwUDBOBggrBgEFBQcwAoZCaHR0cDov
// SIG // L3d3dy5taWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNS
// SIG // b29DZXJBdXQyMDExXzIwMTFfMDNfMjIuY3J0MIGfBgNV
// SIG // HSAEgZcwgZQwgZEGCSsGAQQBgjcuAzCBgzA/BggrBgEF
// SIG // BQcCARYzaHR0cDovL3d3dy5taWNyb3NvZnQuY29tL3Br
// SIG // aW9wcy9kb2NzL3ByaW1hcnljcHMuaHRtMEAGCCsGAQUF
// SIG // BwICMDQeMiAdAEwAZQBnAGEAbABfAHAAbwBsAGkAYwB5
// SIG // AF8AcwB0AGEAdABlAG0AZQBuAHQALiAdMA0GCSqGSIb3
// SIG // DQEBCwUAA4ICAQBn8oalmOBUeRou09h0ZyKbC5YR4WOS
// SIG // mUKWfdJ5DJDBZV8uLD74w3LRbYP+vj/oCso7v0epo/Np
// SIG // 22O/IjWll11lhJB9i0ZQVdgMknzSGksc8zxCi1LQsP1r
// SIG // 4z4HLimb5j0bpdS1HXeUOeLpZMlEPXh6I/MTfaaQdION
// SIG // 9MsmAkYqwooQu6SpBQyb7Wj6aC6VoCo/KmtYSWMfCWlu
// SIG // WpiW5IP0wI/zRive/DvQvTXvbiWu5a8n7dDd8w6vmSiX
// SIG // mE0OPQvyCInWH8MyGOLwxS3OW560STkKxgrCxq2u5bLZ
// SIG // 2xWIUUVYODJxJxp/sfQn+N4sOiBpmLJZiWhub6e3dMNA
// SIG // BQamASooPoI/E01mC8CzTfXhj38cbxV9Rad25UAqZaPD
// SIG // XVJihsMdYzaXht/a8/jyFqGaJ+HNpZfQ7l1jQeNbB5yH
// SIG // PgZ3BtEGsXUfFL5hYbXw3MYbBL7fQccOKO7eZS/sl/ah
// SIG // XJbYANahRr1Z85elCUtIEJmAH9AAKcWxm6U/RXceNcbS
// SIG // oqKfenoi+kiVH6v7RyOA9Z74v2u3S5fi63V4GuzqN5l5
// SIG // GEv/1rMjaHXmr/r8i+sLgOppO6/8MO0ETI7f33VtY5E9
// SIG // 0Z1WTk+/gFcioXgRMiF670EKsT/7qMykXcGhiJtXcVZO
// SIG // SEXAQsmbdlsKgEhr/Xmfwb1tbWrJUnMTDXpQzTGCGY8w
// SIG // ghmLAgEBMIGVMH4xCzAJBgNVBAYTAlVTMRMwEQYDVQQI
// SIG // EwpXYXNoaW5ndG9uMRAwDgYDVQQHEwdSZWRtb25kMR4w
// SIG // HAYDVQQKExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xKDAm
// SIG // BgNVBAMTH01pY3Jvc29mdCBDb2RlIFNpZ25pbmcgUENB
// SIG // IDIwMTECEzMAAAJSizOq+JXzOdsAAAAAAlIwDQYJYIZI
// SIG // AWUDBAIBBQCgga4wGQYJKoZIhvcNAQkDMQwGCisGAQQB
// SIG // gjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcC
// SIG // ARUwLwYJKoZIhvcNAQkEMSIEIAtdr7zd+3GWgbZUJDjp
// SIG // lDM3M900rNZq/1GYJjRPZT8JMEIGCisGAQQBgjcCAQwx
// SIG // NDAyoBSAEgBNAGkAYwByAG8AcwBvAGYAdKEagBhodHRw
// SIG // Oi8vd3d3Lm1pY3Jvc29mdC5jb20wDQYJKoZIhvcNAQEB
// SIG // BQAEggEAc9SwCAcVEcgBkxuH4S0LsfxIbGIYPb0xTdFS
// SIG // J4l4dr1vJqSjQioS6L2ZQBrsNNNvSU3VOjPNkmRN4Av1
// SIG // f87w4mOWMVuLFwXUhNT8ZKtsyNeWPXUkegZ7V6bPZtfM
// SIG // r8SVRak3tp9kT/OEY5MJQ5wN0NSumCwtxRRlko9bk/FW
// SIG // Gs+cVkIRlvLEithfTZKs4W+jqHEVVD4Pj0k8dWawc1HQ
// SIG // A9JGtmoHOZvYjL1SupttnH5m10TtgYCfYT3BHtaJGS2B
// SIG // lnlwAmal+OqCIGf1uCc88K7jix+6uFqsGIzB57ShJAXU
// SIG // UIxYNKc8zEENDQjH85bhsb69f5z7adNN0Tf6tiPPzKGC
// SIG // FxkwghcVBgorBgEEAYI3AwMBMYIXBTCCFwEGCSqGSIb3
// SIG // DQEHAqCCFvIwghbuAgEDMQ8wDQYJYIZIAWUDBAIBBQAw
// SIG // ggFZBgsqhkiG9w0BCRABBKCCAUgEggFEMIIBQAIBAQYK
// SIG // KwYBBAGEWQoDATAxMA0GCWCGSAFlAwQCAQUABCAUHm1a
// SIG // +DkJ6DxePsoBvqbChz/f1i5kSILf6PL+RIGiwQIGYcIf
// SIG // Q+eVGBMyMDIyMDEyMjA3Mjg0Ni4yODNaMASAAgH0oIHY
// SIG // pIHVMIHSMQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2Fz
// SIG // aGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UE
// SIG // ChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMS0wKwYDVQQL
// SIG // EyRNaWNyb3NvZnQgSXJlbGFuZCBPcGVyYXRpb25zIExp
// SIG // bWl0ZWQxJjAkBgNVBAsTHVRoYWxlcyBUU1MgRVNOOjhE
// SIG // NDEtNEJGNy1CM0I3MSUwIwYDVQQDExxNaWNyb3NvZnQg
// SIG // VGltZS1TdGFtcCBTZXJ2aWNloIIRaDCCBxQwggT8oAMC
// SIG // AQICEzMAAAGILs3GgUHhvCoAAQAAAYgwDQYJKoZIhvcN
// SIG // AQELBQAwfDELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldh
// SIG // c2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNV
// SIG // BAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjEmMCQGA1UE
// SIG // AxMdTWljcm9zb2Z0IFRpbWUtU3RhbXAgUENBIDIwMTAw
// SIG // HhcNMjExMDI4MTkyNzQwWhcNMjMwMTI2MTkyNzQwWjCB
// SIG // 0jELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEtMCsGA1UECxMkTWlj
// SIG // cm9zb2Z0IElyZWxhbmQgT3BlcmF0aW9ucyBMaW1pdGVk
// SIG // MSYwJAYDVQQLEx1UaGFsZXMgVFNTIEVTTjo4RDQxLTRC
// SIG // RjctQjNCNzElMCMGA1UEAxMcTWljcm9zb2Z0IFRpbWUt
// SIG // U3RhbXAgU2VydmljZTCCAiIwDQYJKoZIhvcNAQEBBQAD
// SIG // ggIPADCCAgoCggIBAJrnEAgEJpHFx8g61eEvPFXiYNlx
// SIG // qjSnFqbK2qUShVnIYYy7H/zPVzfW4M5yzePAVzwLTpcK
// SIG // HnQdpDeG2XTz9ynUTW2KtbTRVIfFJ5owgq/goy5a4oB3
// SIG // JktEfq7DdoATF5SxGYdlvwjrg/VTi7G9j9ow6eN91eK1
// SIG // AAFFvNjO64PNXdznHLTvtV1tYdxLW0LUukBJMOg2CLr3
// SIG // 1+wMPI1x2Z7DLoD/GQNaLaa6UzVIf80Vguwicgc8pkCA
// SIG // 0gnVoVXw+LIcXvkbOtWsX9u204OR/1f0pDXfYczOjav8
// SIG // tjowyqy7bjfYUud+evboUzUHgIQFQ33h6RM5TL7Vzsl+
// SIG // jE5nt45x3Rz4+hi0/QDESKwH/eoT2DojxAbx7a4OjKYi
// SIG // N/pejZW0jrNevxU3pY09frHbFhrRU2b3mvaQKldWge/e
// SIG // Wg5JmerEZuY7XZ1Ws36Fqx3d7w3od+VldPL1uE5TnxHF
// SIG // dvim2oqz8WhZCePrZbCfjH7FTok6/2Zw4GjGh5886IHp
// SIG // SNwKHw1PSE2zJE7U8ayz8oE20XbW6ba5y8wZ9o80eEyX
// SIG // 5EKPoc1rmjLuTrTGYildiOTDtJtZirlAIKKvuONi8PAk
// SIG // Lo/RAthfJ02yW9jXFA4Pu+HYCYrPz/AWvzq5cVvk64HO
// SIG // kzxsQjrU+9/VKnrJb1g+qzUOlBDvX+71g5IXdr7bAgMB
// SIG // AAGjggE2MIIBMjAdBgNVHQ4EFgQUZHm1UMSju867vfqN
// SIG // uxoz5YzJSkowHwYDVR0jBBgwFoAUn6cVXQBeYl2D9OXS
// SIG // ZacbUzUZ6XIwXwYDVR0fBFgwVjBUoFKgUIZOaHR0cDov
// SIG // L3d3dy5taWNyb3NvZnQuY29tL3BraW9wcy9jcmwvTWlj
// SIG // cm9zb2Z0JTIwVGltZS1TdGFtcCUyMFBDQSUyMDIwMTAo
// SIG // MSkuY3JsMGwGCCsGAQUFBwEBBGAwXjBcBggrBgEFBQcw
// SIG // AoZQaHR0cDovL3d3dy5taWNyb3NvZnQuY29tL3BraW9w
// SIG // cy9jZXJ0cy9NaWNyb3NvZnQlMjBUaW1lLVN0YW1wJTIw
// SIG // UENBJTIwMjAxMCgxKS5jcnQwDAYDVR0TAQH/BAIwADAT
// SIG // BgNVHSUEDDAKBggrBgEFBQcDCDANBgkqhkiG9w0BAQsF
// SIG // AAOCAgEAQBBa2/tYCCbL/xii0ts2r5tnpNe+5pOMrugb
// SIG // kulYiLi9HttGDdnXV3olIRHYZNxUbaPxg/d5OUiMjSel
// SIG // /qfLkDDsSNt2DknchMyycIe/n7btCH/Mt8egCdEtXddj
// SIG // me37GKpYx1HnHJ3kvQ1qoqR5PLjPJtmWwYUZ1DfDOIqo
// SIG // OK6CRpmSmfRXPGe2RyYDPe4u3yMgPYSR9Ne89uVqwyZc
// SIG // WqQ+XZjMjcs83wFamgcnpgqAZ+FZEQhjSEsdMUZXG/d1
// SIG // uhDYSRdTQYzJd3ClRB1uHfGNDWYaXVw7Xi5PR4Gycngi
// SIG // NnzfRgawktQdWpPtfeDxomSi/PoLSuzaKwKADELxZGIK
// SIG // x61gmH41ej6LgtzfgOsDga3JFTh0/T1CAyuQAwh+Ga2k
// SIG // InXkvSw/4pihzNyOImsz5KHB3BRwfcqOXfZTCWfqZwAF
// SIG // oJUEIzFoVKpxP5ZQPhKo2ztJQMZZlLVYqFVLMIU96Sug
// SIG // 4xUVzPy1McE7bbn89cwYxC5ESGfLgstWJDMXwRcBKLP0
// SIG // BSJQ2hUr1J+CIlmQN1S3wBI8udYicCto0iB8PtW4wiPh
// SIG // QR3Ak0R9qT9/oeQ5UOQGf3b3HzawEz9cMM9uSK/CoCjm
// SIG // x0QiGB+FSNla5jm6EhxRu/SWx3ZD1Uo3y8U7k7KIeRc6
// SIG // FNbebqxtK8LpaGWRWcU5K8X8k5Ib5owwggdxMIIFWaAD
// SIG // AgECAhMzAAAAFcXna54Cm0mZAAAAAAAVMA0GCSqGSIb3
// SIG // DQEBCwUAMIGIMQswCQYDVQQGEwJVUzETMBEGA1UECBMK
// SIG // V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
// SIG // A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMTIwMAYD
// SIG // VQQDEylNaWNyb3NvZnQgUm9vdCBDZXJ0aWZpY2F0ZSBB
// SIG // dXRob3JpdHkgMjAxMDAeFw0yMTA5MzAxODIyMjVaFw0z
// SIG // MDA5MzAxODMyMjVaMHwxCzAJBgNVBAYTAlVTMRMwEQYD
// SIG // VQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQHEwdSZWRtb25k
// SIG // MR4wHAYDVQQKExVNaWNyb3NvZnQgQ29ycG9yYXRpb24x
// SIG // JjAkBgNVBAMTHU1pY3Jvc29mdCBUaW1lLVN0YW1wIFBD
// SIG // QSAyMDEwMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIIC
// SIG // CgKCAgEA5OGmTOe0ciELeaLL1yR5vQ7VgtP97pwHB9Kp
// SIG // bE51yMo1V/YBf2xK4OK9uT4XYDP/XE/HZveVU3Fa4n5K
// SIG // Wv64NmeFRiMMtY0Tz3cywBAY6GB9alKDRLemjkZrBxTz
// SIG // xXb1hlDcwUTIcVxRMTegCjhuje3XD9gmU3w5YQJ6xKr9
// SIG // cmmvHaus9ja+NSZk2pg7uhp7M62AW36MEBydUv626GIl
// SIG // 3GoPz130/o5Tz9bshVZN7928jaTjkY+yOSxRnOlwaQ3K
// SIG // Ni1wjjHINSi947SHJMPgyY9+tVSP3PoFVZhtaDuaRr3t
// SIG // pK56KTesy+uDRedGbsoy1cCGMFxPLOJiss254o2I5Jas
// SIG // AUq7vnGpF1tnYN74kpEeHT39IM9zfUGaRnXNxF803RKJ
// SIG // 1v2lIH1+/NmeRd+2ci/bfV+AutuqfjbsNkz2K26oElHo
// SIG // vwUDo9Fzpk03dJQcNIIP8BDyt0cY7afomXw/TNuvXsLz
// SIG // 1dhzPUNOwTM5TI4CvEJoLhDqhFFG4tG9ahhaYQFzymei
// SIG // XtcodgLiMxhy16cg8ML6EgrXY28MyTZki1ugpoMhXV8w
// SIG // dJGUlNi5UPkLiWHzNgY1GIRH29wb0f2y1BzFa/ZcUlFd
// SIG // Etsluq9QBXpsxREdcu+N+VLEhReTwDwV2xo3xwgVGD94
// SIG // q0W29R6HXtqPnhZyacaue7e3PmriLq0CAwEAAaOCAd0w
// SIG // ggHZMBIGCSsGAQQBgjcVAQQFAgMBAAEwIwYJKwYBBAGC
// SIG // NxUCBBYEFCqnUv5kxJq+gpE8RjUpzxD/LwTuMB0GA1Ud
// SIG // DgQWBBSfpxVdAF5iXYP05dJlpxtTNRnpcjBcBgNVHSAE
// SIG // VTBTMFEGDCsGAQQBgjdMg30BATBBMD8GCCsGAQUFBwIB
// SIG // FjNodHRwOi8vd3d3Lm1pY3Jvc29mdC5jb20vcGtpb3Bz
// SIG // L0RvY3MvUmVwb3NpdG9yeS5odG0wEwYDVR0lBAwwCgYI
// SIG // KwYBBQUHAwgwGQYJKwYBBAGCNxQCBAweCgBTAHUAYgBD
// SIG // AEEwCwYDVR0PBAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8w
// SIG // HwYDVR0jBBgwFoAU1fZWy4/oolxiaNE9lJBb186aGMQw
// SIG // VgYDVR0fBE8wTTBLoEmgR4ZFaHR0cDovL2NybC5taWNy
// SIG // b3NvZnQuY29tL3BraS9jcmwvcHJvZHVjdHMvTWljUm9v
// SIG // Q2VyQXV0XzIwMTAtMDYtMjMuY3JsMFoGCCsGAQUFBwEB
// SIG // BE4wTDBKBggrBgEFBQcwAoY+aHR0cDovL3d3dy5taWNy
// SIG // b3NvZnQuY29tL3BraS9jZXJ0cy9NaWNSb29DZXJBdXRf
// SIG // MjAxMC0wNi0yMy5jcnQwDQYJKoZIhvcNAQELBQADggIB
// SIG // AJ1VffwqreEsH2cBMSRb4Z5yS/ypb+pcFLY+TkdkeLEG
// SIG // k5c9MTO1OdfCcTY/2mRsfNB1OW27DzHkwo/7bNGhlBgi
// SIG // 7ulmZzpTTd2YurYeeNg2LpypglYAA7AFvonoaeC6Ce57
// SIG // 32pvvinLbtg/SHUB2RjebYIM9W0jVOR4U3UkV7ndn/OO
// SIG // PcbzaN9l9qRWqveVtihVJ9AkvUCgvxm2EhIRXT0n4ECW
// SIG // OKz3+SmJw7wXsFSFQrP8DJ6LGYnn8AtqgcKBGUIZUnWK
// SIG // NsIdw2FzLixre24/LAl4FOmRsqlb30mjdAy87JGA0j3m
// SIG // Sj5mO0+7hvoyGtmW9I/2kQH2zsZ0/fZMcm8Qq3UwxTSw
// SIG // ethQ/gpY3UA8x1RtnWN0SCyxTkctwRQEcb9k+SS+c23K
// SIG // jgm9swFXSVRk2XPXfx5bRAGOWhmRaw2fpCjcZxkoJLo4
// SIG // S5pu+yFUa2pFEUep8beuyOiJXk+d0tBMdrVXVAmxaQFE
// SIG // fnyhYWxz/gq77EFmPWn9y8FBSX5+k77L+DvktxW/tM4+
// SIG // pTFRhLy/AsGConsXHRWJjXD+57XQKBqJC4822rpM+Zv/
// SIG // Cuk0+CQ1ZyvgDbjmjJnW4SLq8CdCPSWU5nR0W2rRnj7t
// SIG // fqAxM328y+l7vzhwRNGQ8cirOoo6CGJ/2XBjU02N7oJt
// SIG // pQUQwXEGahC0HVUzWLOhcGbyoYIC1zCCAkACAQEwggEA
// SIG // oYHYpIHVMIHSMQswCQYDVQQGEwJVUzETMBEGA1UECBMK
// SIG // V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
// SIG // A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMS0wKwYD
// SIG // VQQLEyRNaWNyb3NvZnQgSXJlbGFuZCBPcGVyYXRpb25z
// SIG // IExpbWl0ZWQxJjAkBgNVBAsTHVRoYWxlcyBUU1MgRVNO
// SIG // OjhENDEtNEJGNy1CM0I3MSUwIwYDVQQDExxNaWNyb3Nv
// SIG // ZnQgVGltZS1TdGFtcCBTZXJ2aWNloiMKAQEwBwYFKw4D
// SIG // AhoDFQDhPIrMfCAXlT0sHg/NOZeUHXoOQqCBgzCBgKR+
// SIG // MHwxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xJjAkBgNVBAMTHU1p
// SIG // Y3Jvc29mdCBUaW1lLVN0YW1wIFBDQSAyMDEwMA0GCSqG
// SIG // SIb3DQEBBQUAAgUA5ZYj8jAiGA8yMDIyMDEyMjE0MzQy
// SIG // NloYDzIwMjIwMTIzMTQzNDI2WjB3MD0GCisGAQQBhFkK
// SIG // BAExLzAtMAoCBQDlliPyAgEAMAoCAQACAgiKAgH/MAcC
// SIG // AQACAhE7MAoCBQDll3VyAgEAMDYGCisGAQQBhFkKBAIx
// SIG // KDAmMAwGCisGAQQBhFkKAwKgCjAIAgEAAgMHoSChCjAI
// SIG // AgEAAgMBhqAwDQYJKoZIhvcNAQEFBQADgYEAw/fBjH/J
// SIG // 0oVV9WlRl4sQm0TmnoHGBmDrWAb6o25c11moQ5AHu1np
// SIG // WEfMD49RGUfaq9yQ2maZZEOIdE4SYyJ96ktFuT2KS+Ar
// SIG // yrCs4IM9qj7E1klpkVy3MJvBYYgraz7WPNmm3V89BUC7
// SIG // 8/ZW9ocvaz3/nGy1SEovyg795BmXKQQxggQNMIIECQIB
// SIG // ATCBkzB8MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2Fz
// SIG // aGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UE
// SIG // ChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSYwJAYDVQQD
// SIG // Ex1NaWNyb3NvZnQgVGltZS1TdGFtcCBQQ0EgMjAxMAIT
// SIG // MwAAAYguzcaBQeG8KgABAAABiDANBglghkgBZQMEAgEF
// SIG // AKCCAUowGgYJKoZIhvcNAQkDMQ0GCyqGSIb3DQEJEAEE
// SIG // MC8GCSqGSIb3DQEJBDEiBCAXGxmBmVHv7ROYPjpEBKsS
// SIG // 4vaQGni6LNSGqF2e5lH8yzCB+gYLKoZIhvcNAQkQAi8x
// SIG // geowgecwgeQwgb0EIGbp3u2sBjdGhIL4z+ycjtzSpe4b
// SIG // LV/AoYaypl7SSUClMIGYMIGApH4wfDELMAkGA1UEBhMC
// SIG // VVMxEzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcT
// SIG // B1JlZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jw
// SIG // b3JhdGlvbjEmMCQGA1UEAxMdTWljcm9zb2Z0IFRpbWUt
// SIG // U3RhbXAgUENBIDIwMTACEzMAAAGILs3GgUHhvCoAAQAA
// SIG // AYgwIgQgmwh2uI6ri8kxfouFDTY/V46qABtbiGSsj9Rd
// SIG // wXYnOZMwDQYJKoZIhvcNAQELBQAEggIAlgDxfTI0ont1
// SIG // d/fx30Niga5IoySd6k9T09WLrOdoU8AT5iUrGtuyK6cz
// SIG // EN09mkoLJDxi9Ab6nPExDEA/rcw3TdO9s/Lsy/6udxvO
// SIG // R3XG17mzuZ2qTkfzo/g8TkMO7cgzhCHbnW/MTaLvHx+c
// SIG // YznGV8R3ar4v9YQJDpLXoSlLZM5LLnwlYA/a/6LJzD2N
// SIG // ug6mOw3EiNjWfZngioci2y5e7WftbF1hAzyKIwrCn2u7
// SIG // VcTXIgovTOdmxg5nK0dwxopQ9FLC5xsfI+mRrOpvraeQ
// SIG // g4TY71hzoB2Q4z1aTZwkzSh5yHOClFwC0d9WKX1VaJ6k
// SIG // QbR9SMZOBxwXps5vaB5QaPyfZXe4Ozwpe2lqTqNrwjY9
// SIG // oD53ja1zQyhOo2Z6mWP/tXg7cVK/im45Y7OXkTRjuFls
// SIG // 1tenPYWz80AqC/SiBMGEPYV98tyKiOh4jJYcrZFE9nuZ
// SIG // IbkrSroIt3WyhLKTHdZuYPudcM8a4rtzYEMCMXQHH1AM
// SIG // kLz1WIpFqB9k3n2ExcJNedhGEs6I60dxXI6UF/pmD+Oo
// SIG // Gh/R1yFAVLtMafcRruy87kqP5LZBeDt896H9ybspXExm
// SIG // 7als4GTZK9soPzOjX5jGQ9LoQvcIzmm9Pa7qT5S+Yt88
// SIG // xHYOgTvC+LCHHd6jQ0BAqirCCKv1/yRypHjsKMIK6zQR
// SIG // 7yOG2p05d+k=
// SIG // End signature block
